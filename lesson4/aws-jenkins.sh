#!/bin/bash

#  System status

echo "volume Usage:"
aws ec2 describe-volumes --output table

echo "vpcs:"
aws ec2 describe-vpcs  --output table

echo "organization:"
aws organizations describe-organization  --output table

echo "subnet:"
aws ec2 describe-subnets  --output table

echo "profile:"
aws iam list-users --profile default --output table

echo "s3:"
aws s3 ls --profile default --output table



aws ec2 describe-instances --output table

echo ""
 
aws route53 list-hosted-zones --output table

echo ""


