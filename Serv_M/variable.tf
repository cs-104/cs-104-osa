

variable "user" {
  type        = string
  description = "The description of the IAM user"
  default = ""
}

# variable "policy" {
#   type        = string
#   description = "The description of the IAM policy"
#   default = "poll"
# }

variable "availability_zone_names" {
  type    = string
  default = "eu-west-1"
}