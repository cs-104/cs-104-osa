variable "region" {
  type = string
  default = "us-east-1"
}

variable "image_id" {
  type = string
  default = "ami-02b4e72b17337d6c1"
}
variable "aws_vpc" {
  type = string
  default = "10.0.0.0/16"
}
variable "aws_subnets" {
  type = string
  default = "10.0.0.0/24"
}

#variable "mactype" {
 # type = string
  #default = "t2.micro"
#}

variable "ec2_instance_port" {
  type = number
  default = 80
}

