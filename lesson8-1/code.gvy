							
                     									
              pipeline {
  agent {
    label 'master'
  }

    environment {
		PATH = "$HOME/.tfenv/bin:$PATH"
    } 
    
    parameters {
		booleanParam(name: 'SANDBOX',  defaultValue: false)
        booleanParam(name: 'PREPROD' , defaultValue: false)
        booleanParam(name: 'PROD', defaultValue: false)
		choice(name: 'CREATE_USER', choices: ['No', 'Yes'], description: 'If you want to create a text user on the server')
		choice(name: 'READ_USER', choices: ['No', 'Yes'], description: 'If you want to read a user profile from the server')
        choice(name: 'UPDATE_USER', choices: ['No', 'Yes'], description: 'If you want to update the user profile on the server')
        choice(name: 'DELETE_USER', choices: ['No', 'Yes'], description: ' If you want to delete the user profile from server')
        choice(name: 'DESTROY_S3', choices: ['No', 'Yes'], description: 'Choice to destroy s3 bucket')
        choice(name: 'DESTROY_USER', choices: ['No', 'Yes'], description: 'Choice to destroy user')
		
    }
	
	stages {
        stage('Parameters Services') {
            steps {
                echo "Select the individual environment you want to run the report."
                echo "Select sandbox service ?: ${params.SANDBOX}"
                echo "Select preprod service ?: ${params.PREPROD}"
                echo "Select prod service ?: ${params.PROD}"
                echo "Create a text report: ${params.CREATE_USER}" 
				echo "Create a text report: ${params.READ_USER}"
                echo "Create a text report: ${params.UPDATE_USER}"
                echo "Create a text report: ${params.DELETE_USER}"
                echo "Do you want to destroy s3: ${params.DESTROY_S3}" 
				echo "Do you want to destroy user: ${params.DESTROY_USER}" 
            }
        }
    
        stage('Multiple account Authentication') {
            steps {
			 script {
               sh """
				tfenv use 1.0.4
				tfenv use 1.0.4
				aws s3 ls 
                aws s3 ls --profile sandbox
                aws s3 ls --profile preprod
                aws s3 ls --profile prod
             """
	         }
            }
	    }
	
        stage('SANDBOX'){
            when { 
                expression {params.SANDBOX} 
            }
            steps {
                script {
      
                        dir("./lesson8-3-code.gvy/S3") {
                            sh """ 
                            terraform workspace select default
                       terraform workspace select sandbox  || terraform workspace new sandbox							
                       
                    					 
                            
                            terraform init -input=false
                            terraform plan -out sandbox_plan -input=false
                            terraform apply -auto-approve sandbox_plan
                            """
							
							     if(params.DESTROY_S3 == 'Yes' ){ 
                                
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select sandbox  || terraform workspace new sandbox							   
                                    terraform destroy -auto-approve
                                    """
                                }
                            
                           if(params.CREATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select sandbox  || terraform workspace new sandbox							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out sandbox_plan -input=false
                                    terraform apply -auto-approve sandbox_plan
                                    """
                                }
                            }
                           if(params.READ_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select sandbox  || terraform workspace new sandbox							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out sandbox_plan -input=false
                                    terraform apply -auto-approve sandbox_plan
                                    """
                                }
                            }
                            if(params.UPDATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                     terraform workspace select sandbox  || terraform workspace new sandbox							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out sandbox_plan -input=false
                                    terraform apply -auto-approve sandbox_plan
                                    """
                                }
                            }
                            if(params.DELETE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select sandbox  || terraform workspace new sandbox							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out sandbox_plan -input=false
                                    terraform apply -auto-approve sandbox_plan
                                    """
                                }
                            }
                         if(params.DESTROY_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                   terraform workspace select default
                                    terraform workspace select sandbox  || terraform workspace new sandbox							   
                                    terraform destroy -auto-approve
                                    
                                    """
                                }
                            } 							
                        }
                    }
                }
               
        }

       stage('PREPROD'){
            when { 
                expression {params.PREPROD} 
            }
            steps {
                script {
      
                        dir("./lesson8-3-code.gvy/S3") {
                            sh """
                            terraform workspace select default
                             terraform workspace select preprod  || terraform workspace new preprod  							
                           
                            terraform init -input=false
                            terraform plan -out preprod_plan -input=false
                            terraform apply -auto-approve preprod_plan
                            """
                        
							   if(params.DESTROY_S3 == 'Yes' ){ 
                                
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select preprod  || terraform workspace new preprod							   
                                    terraform destroy -auto-approve
                                    """
                                }
                            
                             if(params.CREATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select preprod  || terraform workspace new preprod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out preprod_plan -input=false
                                    terraform apply -auto-approve preprod_plan
                                    """
                                }
                            }
                            if(params.READ_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select preprod  || terraform workspace new preprod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out preprod_plan -input=false
                                    terraform apply -auto-approve preprod_plan
                                    """
                                }
                            }
                            if(params.UPDATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                          terraform workspace select preprod  || terraform workspace new preprod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out preprod_plan -input=false
                                    terraform apply -auto-approve preprod_plan
                                    """
                                }
                            }
                            if(params.DELETE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select preprod  || terraform workspace new preprod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out preprod_plan -input=false
                                    terraform apply -auto-approve preprod_plan
                                    """
                                }
                            }

                           if(params.DESTROY_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                   terraform workspace select default
                                    terraform workspace select preprod  || terraform workspace new preprod							   
                                    terraform destroy -auto-approve
                                    
                                    """
                                }
                            }
                                
                              
                        }
                    }
                }
               
        }
		
		   stage('PROD'){
            when { 
                expression {params.PROD} 
            }
            steps {
                script {
      
                        dir("./lesson8-3-code.gvy/S3") {
                            sh """ 
                             terraform workspace select prod  || terraform workspace new prod  							
                           
                            terraform init -input=false
                            terraform plan -out prod_plan -input=false
                            terraform apply -auto-approve prod_plan
                            """
                       
                                   
							if(params.DESTROY_S3 == 'Yes' ){ 
                                
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							   
                                    terraform destroy -auto-approve
                                    """
                                }
                            
                             if(params.CREATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out prod_plan -input=false
                                    terraform apply -auto-approve prod_plan
                                    """
                                }
                            }
                            if(params.READ_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out prod_plan -input=false
                                    terraform apply -auto-approve prod_plan
                                    """
                                }
                            }
                            if(params.UPDATE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out prod_plan -input=false
                                    terraform apply -auto-approve prod_plan
                                    """
                                }
                            }
                            if(params.DELETE_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                    terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							
                     									
                                   
                                    terraform init -input=false
                                    terraform plan -out prod_plan -input=false
                                    terraform apply -auto-approve prod_plan
                                    """
                                }
                            }

                           if(params.DESTROY_USER == 'Yes' ) {
                                dir("../main") {
                                    sh """ 
                                    ls -a
                                   terraform workspace select default
                                    terraform workspace select prod  || terraform workspace new prod							   
                                    terraform destroy -auto-approve
                                    
                                    """
                                }
                            }
                                
                            }  
                        }
                    }
                }
               
        }

   	
    }    


			
			
			
			
			
                          