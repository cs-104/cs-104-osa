// export AWS_ACCESS_KEY_ID=<your access key>
// export AWS_SECRET_ACCESS_KEY=<your secret key>

resource "aws_instance" "mainprod" {
  instance_type = "t2.micro"      
  ami = "ami-0000bebe516f304b1"
  key_name = "osa-104"
  # key_name = "TerraformProject"   

  tags = {
    Name = "cs-104-lesson6-igie"
  }
}
#resource "aws_key_pair" "igekey" {
#key_name = "igekey"
#public_key = file("id_rsa.pub")
#}
#Create the VPC
 resource "aws_vpc" "New-cs-104" {                # Creating VPC here
   cidr_block       = "172.30.0.0/16"    # Defining the CIDR block use 10.0.0.0/16 for main
   instance_tenancy = "default"
   tags = {
    Name = "cs-104-lesson6-osa"
    }
  }
 #Create a Public Subnets.
 resource "aws_subnet" "publicsubnets1" {    # Creating Public Subnets
   vpc_id =  aws_vpc.New-cs-104.id
   cidr_block = "172.30.1.0/24"        # CIDR block of public subnets
   #map_pulic_ip_on_lunch = true
 
  tags = {
    Name = "PublcSubnet1"
   } 
 
  #ami                    = "ami-ebd02392"
  #instance_type          = "t2.micro"
  #key_name               = "user1"
  #monitoring             = true
  #vpc_security_group_ids = ["sg-12345678"]
  #subnet_id              = "subnet-eddcdzz4"

  #tags = {
  #  Terraform   = "true"
   # Environment = "dev"
  #}
 
 
 
 }
#Create a Public Subnets.
 resource "aws_subnet" "publicsubnets2" {    # Creating Public Subnets
   vpc_id =  aws_vpc.New-cs-104.id
   cidr_block = "172.30.2.0/24"        # CIDR block of public subnets
   #map_pulic_ip_on_lunch = true
 
  tags = {
    Name = "PublcSubnet2"
   } 
 }
 #Create a Public Subnets.
 resource "aws_subnet" "privatsubnets1" {    # Creating Private Subnets
   vpc_id =  aws_vpc.New-cs-104.id
   cidr_block = "172.30.6.0/24"        # CIDR block of private subnets
   #map_pulic_ip_on_lunch = true
 
  tags = {
    Name = "PrivateSubnet1"
   } 
 }
 #Create a Private Subnet                   # Creating Private Subnets
 resource "aws_subnet" "privatesubnets2" {
   vpc_id =  aws_vpc.New-cs-104.id
   cidr_block = "172.30.7.0/24"          # CIDR block of private subnets
   #map_pulic_ip_on_lunch = true 
 tags = {
    Name = "PrivateSubnet1"
      }
    }
resource "aws_security_group" "alb-sec-group" {
  name = "alb-sec-group"
  description = "Security Group for the ELB (ALB)"      
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP"
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTPS"
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "asg_sec_group" {
  name = "asg_sec_group"
  description = "Security Group for the ASG"
  tags = {
    name = "name"
  }
  // Allow ALL outbound traffic
  egress {
    from_port = 0
    protocol = "-1" // ALL Protocols
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  // Allow Inbound traffic from the ELB Security-Group  
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    security_groups = [aws_security_group.alb-sec-group.id] // Allow Inbound traffic from the ALB Sec-Group     
  }
}




